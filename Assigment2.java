/*
Carlos Valenzuela,	CSC 130
Proff.K 
Assigment 2
The following is a program that will find the sum and the average of 5 integers
*/

import java.util.Scanner;

public class Assigment2 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.println(" Please input 5 integers:");
	
		int total = 0;
		int n;
		float average;
		int counter = 0;
	
		while (counter <5)
		{n = input.nextInt();
		total = total + n;
		counter++;}
	
	average = total/5;
	System.out.println( "The sum is: " + total);
	System.out.println("The average is: "+ average);

	}
}